import template from './sw-theme-manager-detail.html.twig';

Shopware.Component.override('sw-theme-manager-detail', {
    template,

    data() {
        return {
            showImportModal: false,
            showExportConflictModal: false,
            importJsonObject: null
        };
    },

    methods: {
        blurExportSettings() {
            /**
             * @feature
             * - ask if user wants to save theme settings before export
             * - open modal dialog on export with extended options (i.e. individual filename)
            **/

            /**
             * Equal check for initial theme config and current theme config inputs
             */
            if ( JSON.stringify(this.currentThemeConfig) === JSON.stringify(this.currentThemeConfigInitial) ) {
                // is equal
                // download theme settings
                this.blurDownloadSettings()
            } else {
                // is not equal
                // open export conflict modal dialog
                this.blurOpenExportConflictModal()
            }
        },

        blurImportSettings() {
            this.showImportModal = true
        },

        blurModalExportSettings( savedSettings ) {
            console.log(savedSettings)
            this.blurDownloadSettings( savedSettings )
            this.blurCloseExportConflictModal()
        },

        blurDownloadSettings( savedSettings = false ) {
            let currentTimestamp = new Date().getTime()
            let themeConfig = this.currentThemeConfig

            if ( savedSettings == true ) {
                themeConfig = this.currentThemeConfigInitial
            }

            let fileUrl = URL.createObjectURL(
                new Blob(
                    // [this.currentThemeConfig],
                    [JSON.stringify(themeConfig, null, 2)],
                    {
                        type: "application/json"
                    }
                )
            )

            let fileName = [ "theme-settings" ]
            fileName.push( this.theme.name.replace(/\s/g, '-') )
            fileName.push( currentTimestamp )
            fileName.push( "json" )

            const anchor = document.createElement("a")
            anchor.href = fileUrl
            anchor.download = fileName.join(".")

            document.body.appendChild(anchor)
            anchor.click()
            document.body.removeChild(anchor)

            URL.revokeObjectURL(fileUrl)
        },

        blurCloseImportModal() {
            this.showImportModal = false
        },

        blurOpenExportConflictModal() {
            this.showExportConflictModal = true
        },

        blurCloseExportConflictModal() {
            this.showExportConflictModal = false
        },

        async uploadJsonFile( jsonFile ) {
            return await jsonFile.text().then( (result) => {
                this.importJsonObject = JSON.parse(result)    
            })
        },

        blurImportConfig() {
            if ( this.importJsonObject !== null ) {
                this.currentThemeConfig = this.importJsonObject
                this.blurCloseImportModal()
                this.importJsonObject = null 
            }
        },

        blurImportAndSaveConfig() {
            if ( this.importJsonObject !== null ) {
                this.currentThemeConfig = this.importJsonObject
                this.blurCloseImportModal()
                this.importJsonObject = null
                return this.onSave()
            }
        }
    }
});